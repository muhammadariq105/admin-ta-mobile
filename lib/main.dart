
import 'package:admin_mobile/controller/controllerauth.dart';
import 'package:admin_mobile/routes/routes.dart';
import 'package:admin_mobile/view/Loadingpage.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(Main());
}

class Main extends StatelessWidget {
  final klik = Get.put(Authcontroller(), permanent: true);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: klik.stremAuthStatus(),
      // builder: ( context,snapshot) {
      // print(snapshot);
      // if(snapshot.connectionState == ConnectionState.active){}
      //   return GetMaterialApp(
      //     title: "Admin",
      //     debugShowCheckedModeBanner: false,
      //     getPages: adminroute.Page,
      //     initialRoute: snapshot.data != null && snapshot.data!.emailVerified
      //         ? adminroute.Home
      //         : adminroute.Onboarding,
      //   );

      // },
       builder: (context, snapshot) {
          print(snapshot);
          if (snapshot.connectionState == ConnectionState.active) {
            return GetMaterialApp(
                title: "Admin",
                debugShowCheckedModeBanner: false,
                // home: snapshot.data != null ? View() : Login(),
                getPages: adminroute.Page,
                initialRoute:
                    snapshot.data != null && snapshot.data!.emailVerified
                        ? adminroute.Home
                        : adminroute.Onboarding

             
                );
          }
          return LoadingPage();
        }
      
      // debugShowCheckedModeBanner: false,
      // getPages: adminroute.Page,
      // initialRoute: adminroute.Home,
      // home: beranda(),
    );
  }
}
