import 'package:admin_mobile/controller/controllerpenjualan.dart';
import 'package:admin_mobile/models/productmodel.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';

class addbarang extends StatefulWidget {
  @override
  State<addbarang> createState() => _addbarangState();
}

class _addbarangState extends State<addbarang> {
  final controller = Get.find<controllermarket>();

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => controllermarket());
    return Scaffold(
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.all(25.0),
          child: ListView(
            children: [
              const Text(
                "Data Product",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                height: Get.height,
                child: ListView.builder(
                  itemCount: controller.listdata.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: const EdgeInsets.only(top: 18),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all()),
                      width: Get.width,
                      height: 120,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            // Container(
                            //   height: 80,
                            //   width: 80,
                            //   child: Image.network(
                            //       controller.listdata[index]["img"]),
                            //   decoration: BoxDecoration(
                            //       borderRadius: BorderRadius.circular(10),
                            //       border: Border.all()),
                            // ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  controller.listdata[index]["Jdl_brg"],
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                    "Rp.${controller.listdata[index]["Harga_brg"]}"),
                                const SizedBox(
                                  height: 3,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(controller.listdata[index]["Jam_rilis"]),
                              ],
                            ),
                            const SizedBox(
                              width: 80,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                    onPressed: () => null,
                                    child: const Text("Edit")),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        var url = Uri.parse(
                                            "http://192.168.1.11/backendphp/delete.php");
                                        var body = {
                                          "id_brg": controller.listdata[index]
                                              ['id_brg']
                                        };

                                        http
                                            .post(url, body: body)
                                            .then((response) {
                                          // Handle response here
                                          print(response.statusCode);
                                        }).catchError((error) {
                                          // Handle error here
                                          print(error);
                                        });
                                      });
                                    },
                                    child: const Text("Delete"))
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add_chart_outlined),
        onPressed: () => Get.toNamed('/Add-barang'),
      ),
    );
  }
}
