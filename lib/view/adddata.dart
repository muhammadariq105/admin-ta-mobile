import 'dart:io';

import 'package:admin_mobile/controller/controllerpenjualan.dart';
import 'package:admin_mobile/controller/imagecontroller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';

class adddatabrg extends StatefulWidget {
  @override
  State<adddatabrg> createState() => _adddatabrgState();
}

class _adddatabrgState extends State<adddatabrg> {
  final add = Get.find<controllermarket>();
  final ImagePickerController imagePickerController =
      Get.put(ImagePickerController());

  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );
    if (picked != null) {
      setState(() {
        add.dateController.text = picked.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah data"),
        centerTitle: true,
        leading: IconButton(
            onPressed: () => Get.back(), icon: Icon(Icons.arrow_back_ios_new)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: [
            TextField(
              controller: add.jdlbrg,
              decoration: InputDecoration(
                  hintText: 'judul barang', labelText: 'judul barang'),
            ),
            SizedBox(
              height: 15,
            ),
            //  TextField(
            //   controller: add.Harga,
            //   decoration: InputDecoration(

            //     hintText: 'Harga',
            //     labelText: 'Harga'
            //   ),
            // ),
            TextField(
              controller: add.Harga,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              decoration: InputDecoration(
                labelText: 'Harga',
                hintText: 'Masukkan Harga',
              ),
            ),
            SizedBox(
              height: 15,
            ),
            // TextField(
            //   controller: add.Jam,
            //   decoration: InputDecoration(
            //       suffixIcon: IconButton(
            //           onPressed: () {
            //             add.selectDate(context);
            //           },
            //           icon: Icon(Icons.date_range)),
            //       hintText: 'Jam',
            //       labelText: 'Jam'),
            // ),
            TextFormField(
              controller: add.dateController,
              readOnly: true, // Membuat TextField hanya dapat dibaca
              decoration: InputDecoration(
                labelText: 'Pilih Tanggal',
                suffixIcon: GestureDetector(
                  onTap: () => selectDate(
                      context), // Tampilkan DatePicker saat ikon diklik
                  child: Icon(Icons.calendar_today),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),

            TextField(
              controller: add.deskripsi,
              decoration: InputDecoration(
                  hintText: 'Deskripsi', labelText: 'Deskripsi'),
            ),
            SizedBox(
              height: 15,
            ),

            Obx(
              () => imagePickerController.selectedImagePath.value.isNotEmpty
                  ? Image.file(
                      File(imagePickerController.selectedImagePath.value),
                      width: 200,
                      height: 200,
                    )
                  : Container(),
            ),
            ElevatedButton(
              onPressed: () => imagePickerController.pickImage(),
              child: Text('Select Image'),
            ),

            ElevatedButton(
                onPressed: () => add.addData(
                    add.jdlbrg,
                    add.Harga,
                    add.Jam,
                    add.deskripsi,
                    add.imagePickerController.selectedImagePath.string),
                child: Text("data"))
          ],
        ),
      ),
    );
  }
}
