import 'package:admin_mobile/controller/controllerauth.dart';
import 'package:admin_mobile/controller/controllerlogin.dart';
import 'package:admin_mobile/view/primary_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

class login extends GetView<logincontroller> {
    final emailC = TextEditingController();
  
  final passwordC = TextEditingController();
  final auth = Get.find<Authcontroller>();
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => logincontroller());
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(70),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 40,
              ),
              Image.asset("assets/images/logo bengkel 88.png", height: 150),
              SizedBox(
                height: 40,
              ),
              Container(
                height: 60,
                width: 260,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 0.5),
                  borderRadius: BorderRadius.circular(18),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: PrimaryTextfield(
                    controller: emailC,
                    hintText: 'Cth. email-kamu@mail.com',
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter your email';
                      } else if (!RegExp("^[a-zA-Z0-9_.-]+@[a-zA-Z]+[.]+[a-z]")
                          .hasMatch(value)) {
                        return 'Please enter valid email';
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                height: 60,
                width: 260,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 0.5),
                  borderRadius: BorderRadius.circular(18),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Obx(() => PrimaryTextfield(
                        hintText: "Masukkan kata sandi",
                        
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter your password';
                          }
                          return null;
                        },
                        controller: passwordC,
                        obscureText: controller.passwordhidden.value,
                        suffixIcon: IconButton(
                          padding: EdgeInsets.zero,
                          icon: controller.passwordhidden.value == true
                              ? const Icon(Icons.visibility_outlined)
                              : const Icon(Icons.visibility_off_outlined),
                          onPressed: () {
                            controller.passwordhidden.toggle();
                          },
                        ),
                      )),
                ),
              ),
              SizedBox(
                height: 100,
              ),
              Text(
                "ADMIN",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              ElevatedButton(
                onPressed: () => auth.login(emailC.text,passwordC.text),
                child: Text(
                  "Login",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xff07B141)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
