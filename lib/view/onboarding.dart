import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Beranda extends StatelessWidget {
  const Beranda({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(70.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 50,
            ),
            Text(
              "88 MOTOR TARUTUNG",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 14,
            ),
            Text("jl.D I Panjaitan No.88 TARUTUNG"),
            SizedBox(
              height: 57,
            ),
            Image.asset("assets/images/logo bengkel 88.png"),
            SizedBox(
              height: 200,
            ),
            Text(
              "ADMIN",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            ElevatedButton(
              onPressed: () => Null,
              child: Text("LOGIN"),
              style:
                  ElevatedButton.styleFrom(backgroundColor: Color(0xff07B141)),
            )
          ],
        ),
      ),
    );
  }
}
