import 'package:admin_mobile/bindings/bindinglogin.dart';
import 'package:admin_mobile/bindings/bindingmarket.dart';
import 'package:admin_mobile/routes/routesname.dart';
import 'package:admin_mobile/view/adddata.dart';
import 'package:admin_mobile/view/beranda.dart';
import 'package:admin_mobile/view/login.dart';
import 'package:admin_mobile/view/pemesananbrg.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';

class adminroute {
  static const Home = routename.Home;
  static const Onboarding ='/login';
  static final Page = [
    GetPage(
        name: routename.Home,
        page: () => beranda(),
        transition: Transition.downToUp),
    GetPage(
        name: '/login',
        page: () => login(),
        binding: LoginBinding(),
        transition: Transition.downToUp),
    GetPage(
        name: '/pemesanan-barang',
        page: () => addbarang(),
        binding: marketbinding(),
        transition: Transition.downToUp),
    GetPage(
        name: '/Add-barang',
        page: () => adddatabrg(),
        binding: marketbinding(),
        transition: Transition.downToUp),
  ];
}
