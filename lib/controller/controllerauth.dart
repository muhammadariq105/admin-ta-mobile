

import 'package:admin_mobile/routes/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class Authcontroller extends GetxController{
 FirebaseAuth auth = FirebaseAuth.instance;

 Stream<User?> stremAuthStatus() {
    return auth.authStateChanges();
  }
 RxBool passwordIsHidden = true.obs;
 
  RxBool isLoading = false.obs;
  final GetStorage _storage = GetStorage();
  final notelp = TextEditingController();
  final alamat = TextEditingController();
  Future signup(
      String email, String password, String address, String nohp) async {
    try {
      // ignore: non_constant_identifier_names
      UserCredential Myuser = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      await Myuser.user?.sendEmailVerification();
    
      Get.defaultDialog(
          title: "Verifikasi email",
          middleText: "Kami telah mengirimkan email verifikasi ke $email",
          onConfirm: () {
            Get.back();
            Get.back();
          },
          textConfirm: "Ya,Saya akan cek email");
      String userId = Myuser.user!.uid;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        Get.defaultDialog(
            title: "Password Error",
            middleText: "Mohon Periksa Kembali password anda",
            onConfirm: () {
              Get.back();
              Get.back();
            },
            textConfirm: "Oke");
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

void logout() async {
    await FirebaseAuth.instance.signOut();
    Get.toNamed('/login');
  }

 void login(String email, String password) async {
    try {
      UserCredential Myuser = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      final user = auth.currentUser;
      if (user != null) {
        List<String?> providerList = [];
        for (final providerProfile in user.providerData) {
          final provider = providerProfile.providerId;
          Get.defaultDialog(
              title: "Terjadi kesalahan", middleText: "Email tidak terdaftar");
          print('PROVIDER ID: $provider');
          providerList.add(provider);
          _storage.write('provider', providerList);
          providerList.add(provider);
          _storage.write('provider', providerList);
        }
        isLoading.value = false;
      }
      if (Myuser.user!.emailVerified) {
        Get.back();
        Get.offAllNamed(adminroute.Home);
      } else {
        Get.defaultDialog(
          title: "Terjadi kesalahan",
          middleText: "Mohon Verifikasi email anda $email",
        );
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        Get.defaultDialog(
            title: "Terjadi kesalahan",
            middleText: "Email tidak ditemukan",
            onConfirm: () {
              Get.back();
            },
            textConfirm: "Oke");
        print('email tidak di temukan');
      } else if (e.code == 'wrong-password') {
        Get.defaultDialog(
            title: "Terjadi kesalahan",
            middleText: "Masukkan password yang valid",
            onConfirm: () {
              Get.back();
            },
            textConfirm: "Oke");
        print('Wrong password provided for that user.');
      }
    }
  }
}